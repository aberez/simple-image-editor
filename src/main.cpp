#include <QApplication>

#include "sie.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    SIE imageEditor;
    imageEditor.show();
    return app.exec();
}
