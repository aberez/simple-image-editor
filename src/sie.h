#ifndef SIE_H
#define SIE_H

#include <QMainWindow>
#include <QtGui>
#include <QtCore/qmath.h>
#include <QString>
#include <QFileDialog>
#include <QMessageBox>
#include <QScrollArea>



class SIE : public QMainWindow
{
    Q_OBJECT

public:
    SIE();

private slots:
   void open();
   void save();

   void linearAdjust();
   void RGBAdjist ();
   void sharpen();
   void gaussianBlur();
   void scaling();
   void rotate();
   void medianFilt();
   void grayWorld();
   void glass();

    void about();

private:
    void createActions();
    void createMenus();
    void updateActions();
    void initializeActions();

    void extractColors();
    void uniteColors();
    void runFilt(QVector< QVector<double> > &matrix);


    int round(double);
    void setVsize(QVector < QVector <double> > & matrix);
    void biLinealInterpol(double x,double y,int &rval,int &gvla,int &bval);
    void makeMedianFilt(QVector < QVector <double> > & ColorM,int radius);

    QLabel *imageLabel;
    QScrollArea *scrollArea;

    QAction *openAct;
    QAction *exitAct;
    QAction *saveAct;

    QAction *linearAdjustAct;
    QAction *RGBAdjistAct;
    QAction *sharpenAct;
    QAction *aboutAct;
    QAction *gaussianBlurAct;
    QAction *scalingAct;
    QAction *rotateAct;
    QAction *medianFiltAct;
    QAction *grayWorldAct;
    QAction *glassAct;



    QMenu *fileMenu;
    QMenu *editMenu;
    QMenu *helpMenu;
    QImage image;
    \

     QVector< QVector<double> > imageR,imageG,imageB;
};

#endif
