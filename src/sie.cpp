

#include "sie.h"


SIE::SIE()
{
    imageLabel = new QLabel;
    imageLabel->setBackgroundRole(QPalette::Base);

    imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    imageLabel->setScaledContents(false);

    scrollArea = new QScrollArea;
    scrollArea->setBackgroundRole(QPalette::Dark);
    scrollArea->setWidget(imageLabel);
    setCentralWidget(scrollArea);
    setWindowTitle("Simple Image Editor");
    resize(640, 480);

    createActions();
    createMenus();
    initializeActions();

}

void SIE::open()//just open .bmp file and show it on the screen
{
    QString fileName = QFileDialog::getOpenFileName(this, "open image", "", "Bitmap Images(*.bmp)");
    if (!fileName.isEmpty()) {
        image.load(fileName);
        if (image.isNull()) {
            QMessageBox::information(this, "SIE","Cannot load this file");
            return;
        }
        imageLabel->setPixmap(QPixmap::fromImage(image));
        imageLabel->resize(imageLabel->pixmap()->size());
        updateActions();
        extractColors();
    }
}

void SIE::save()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Save File", QDir::currentPath(), "Bitmap Images(*.bmp)");
    imageLabel->pixmap()->save(fileName);
}






void SIE::about()
{
    QMessageBox::about(this, "About SIE",
            "<p>The <i>Simple Image Editor (SIE)</i> programm shows "
            "some features of image processing.</p>"
            "<p> Written by <b>Alex Berezin</b>, group 317, CMC, MSU </p>");
}

void SIE::createActions()//create menu action
{
    openAct = new QAction("&Open", this);
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    saveAct = new QAction("&Save", this);
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

    exitAct = new QAction("&Exit", this);
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

    linearAdjustAct=new QAction("&Briteness adjust",this);
    connect(linearAdjustAct,SIGNAL(triggered()), this, SLOT(linearAdjust()));

    RGBAdjistAct=new QAction("&Autolevels",this);
    connect(RGBAdjistAct,SIGNAL(triggered()), this, SLOT(RGBAdjist()));

    sharpenAct=new QAction("&Sharpen",this);
    connect(sharpenAct,SIGNAL(triggered()), this, SLOT(sharpen()));

    gaussianBlurAct=new QAction("&Blur",this);
    connect(gaussianBlurAct,SIGNAL(triggered()), this, SLOT(gaussianBlur()));

    scalingAct=new QAction("&Scale",this);
    connect(scalingAct,SIGNAL(triggered()), this, SLOT(scaling()));

    rotateAct=new QAction("&Rotate",this);
    connect(rotateAct,SIGNAL(triggered()), this, SLOT(rotate()));

    medianFiltAct=new QAction("&Median Filter",this);
    connect(medianFiltAct,SIGNAL(triggered()), this, SLOT(medianFilt()));

    grayWorldAct=new QAction("&Gray World",this);
    connect(grayWorldAct,SIGNAL(triggered()), this, SLOT(grayWorld()));

    glassAct=new QAction("&Glass Effect",this);
    connect(glassAct,SIGNAL(triggered()), this, SLOT(glass()));






    aboutAct = new QAction("&About", this);
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));



}

void SIE::createMenus()
{
    fileMenu = new QMenu("&File", this);
    fileMenu->addAction(openAct);
    fileMenu->addAction(saveAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    editMenu = new QMenu("&Edit", this);
    editMenu->addAction(linearAdjustAct);
    editMenu->addAction(RGBAdjistAct);
    editMenu->addAction(sharpenAct);
    editMenu->addAction(gaussianBlurAct);
    editMenu->addAction(scalingAct);
    editMenu->addAction(rotateAct);
    editMenu->addAction(medianFiltAct);
    editMenu->addAction(grayWorldAct);
    editMenu->addAction(glassAct);




    helpMenu = new QMenu("&Help", this);
    helpMenu->addAction(aboutAct);

    menuBar()->addMenu(fileMenu);
    menuBar()->addMenu(editMenu);
    menuBar()->addMenu(helpMenu);
}

void SIE::initializeActions()
{
saveAct->setEnabled(false);
linearAdjustAct->setEnabled((false));
RGBAdjistAct->setEnabled(false);
sharpenAct->setEnabled(false);
gaussianBlurAct->setEnabled(false);
scalingAct->setEnabled(false);
rotateAct->setEnabled(false);
medianFiltAct->setEnabled(false);
glassAct->setEnabled(false);
grayWorldAct->setEnabled(false);

}

void SIE::updateActions()
{
    saveAct->setEnabled(true);
    linearAdjustAct->setEnabled(true);
    RGBAdjistAct->setEnabled(true);
    sharpenAct->setEnabled(true);
    gaussianBlurAct->setEnabled(true);
    scalingAct->setEnabled(true);
    rotateAct->setEnabled(true);
    medianFiltAct->setEnabled(true);
    glassAct->setEnabled(true);
    grayWorldAct->setEnabled(true);

}



void SIE::setVsize(QVector < QVector <double> > &matrix){//side private function
    matrix.resize(image.height());
    for (int i=0; i<image.height(); i++)
        matrix[i].resize(image.width());
}


void SIE::extractColors(){//get R,G,B matrixes from RGB PixMap
    setVsize(imageR);
    setVsize(imageG);
    setVsize(imageB);


    QRgb pointColor;
    for (int y=0; y<image.height();y++)
        for (int x=0; x<image.width(); x++ ){
            pointColor=image.pixel(x,y);
            imageR[y][x]=qRed(pointColor);
            imageG[y][x]=qGreen(pointColor);
            imageB[y][x]=qBlue(pointColor);

        }


}

int SIE::round(double c){//side function.
    c=qRound(c);
    if (c<0) c=0;
    if (c>255) c=255;
    return c;
}

void SIE::uniteColors(){//convert R,G,B matrixes into RGB pixmap and show
    QRgb pointColor;
    for (int y=0; y<image.height();y++)
        for (int x=0; x<image.width(); x++ ){
            pointColor=qRgb( round(imageR[y][x]),round(imageG[y][x]),round(imageB[y][x]));
            image.setPixel(x,y,pointColor);
        }
    imageLabel->setPixmap(QPixmap::fromImage(image));

}






void SIE::linearAdjust(){ //luminance adjust {1}
     QVector < QVector <double> > imageY,imageU,imageV;
     QVector <int> luma (257);
     setVsize(imageY);
     setVsize(imageU);
     setVsize(imageV);


    for (int y=0; y<image.height();y++)
        for (int x=0; x<image.width(); x++ ){
            imageY[y][x]=0.2126*imageR[y][x]+0.7152*imageG[y][x]+0.0722*imageB[y][x];
            imageU[y][x]=-0.09991*imageR[y][x]-0.33609*imageG[y][x]+0.436*imageB[y][x];
            imageV[y][x]=0.615*imageR[y][x]-0.55861*imageG[y][x]-0.0564*imageB[y][x];
            ++luma[round(imageY[y][x])];
        }
    int imagePsize=image.width()*image.height();
    int lumamin=0, lumamax=255,count=0;
    for (int i=0;i<256;i++){
        count+=luma[i];
        if (lumamin==0 && count>=0.02*imagePsize) lumamin=i;
        if (lumamax==255 && count>=0.97*imagePsize) {lumamax=i; break;}
    }
    if (lumamin==lumamax) return;
    for (int y=0; y<image.height(); y++)
        for (int x=0; x<image.width();x++){
            if (imageY[y][x]<=lumamin) {imageY[y][x]=0; continue;}
            if (imageY[y][x]>=lumamax) {imageY[y][x]=255; continue;}
            imageY[y][x]=(imageY[y][x]-lumamin)*255/(lumamax-lumamin);
        }

    for (int y=0; y<image.height();y++){
            for (int x=0; x<image.width(); x++ ){
                      imageR[y][x]=round(imageY[y][x]+1.28033*imageV[y][x]);
                      imageG[y][x]=round(imageY[y][x]-0.21482*imageU[y][x]-0.38059*imageV[y][x]);
                      imageB[y][x]=round(imageY[y][x]+2.12798*imageU[y][x]);
                   }
         }
    uniteColors();

}


void SIE::RGBAdjist (){ //autolevels {2}
    QVector <int> gistR(256),gistG(256),gistB(256);
    for (int y=0; y<image.height();y++)
        for (int x=0; x<image.width(); x++ ){
            ++gistR[imageR[y][x]];
            ++gistG[imageG[y][x]];
            ++gistB[imageB[y][x]];
        }
    int imagePsize=image.width()*image.height();
    int minR=0,maxR=255,minG=0,maxG=255,minB=0,maxB=255,countR=0,countG=0,countB=0;
    for (int i=0;i<256;i++){
        countR+=gistR[i];
        if (minR==0 && countR>=0.02*imagePsize) minR=i;
        if (maxR==255 && countR>=0.98*imagePsize) maxR=i;

        countG+=gistG[i];
        if (minG==0 && countG>=0.02*imagePsize) minG=i;
        if (maxG==255 && countG>=0.98*imagePsize) maxG=i;

        countB+=gistB[i];
        if (minB==0 && countB>=0.02*imagePsize) minB=i;
        if (maxB==255 && countB>=0.98*imagePsize) maxB=i;
    }
    if (maxR!=minR)
    for (int y=0; y<image.height(); y++)
        for (int x=0; x<image.width();x++){
            if (imageR[y][x]<=minR) imageR[y][x]=0;
            else{
                if (imageR[y][x]>=maxR) imageR[y][x]=255;
                else
                   imageR[y][x]=(imageR[y][x]-minR)*255/(maxR-minR);
            }
        }
    if (maxG!=minG)
    for (int y=0; y<image.height(); y++)
         for (int x=0; x<image.width();x++){
            if (imageG[y][x]<=minG) imageG[y][x]=0;
            else{
                if (imageG[y][x]>=maxG) imageG[y][x]=255;
                else
                   imageG[y][x]=(imageG[y][x]-minG)*255/(maxG-minG);
            }
         }
    if (maxB!=minB)
    for (int y=0; y<image.height(); y++)
        for (int x=0; x<image.width();x++){
            if (imageB[y][x]<=minB) imageB[y][x]=0;
            else{
                if (imageB[y][x]>=maxB) imageB[y][x]=255;
                else
                   imageB[y][x]=(imageB[y][x]-minB)*255/(maxB-minB);
            }
        }
    uniteColors();
}




void SIE::runFilt(QVector< QVector<double> > &matrix){//apply filter's matrix
    QVector < QVector <double> > tempR,tempG,tempB;
    setVsize(tempR);
    setVsize(tempG);
    setVsize(tempB);

    double weight=0;
    int tmp=1543;
    for (int i=0; i<matrix.size(); i++){

        for (int j=0; j<matrix.size(); j++)
            weight+=matrix[i][j];
    }

    int newx,newy;
    for (int y=0; y<image.height(); y++)
        for (int x=0; x<image.width(); x++){
           double meanR=0,meanG=0,meanB=0;
            for (int i=matrix.size()-1; i>=0; i--){
                for (int j=matrix.size()-1; j>=0; j--){
                    newx=x+matrix.size()/2-i;
                    if (newx<-0) newx*=-1;
                    if (newx>=image.width()) newx=2*image.width()-2-newx;

                    newy=y+matrix.size()/2-j;
                    if (newy<-0) newy*=-1;
                    if (newy>=image.height()) newy=2*image.height()-2-newy;


                    meanR+=imageR[newy][newx]*matrix[i][j];
                    meanG+=imageG[newy][newx]*matrix[i][j];
                    meanB+=imageB[newy][newx]*matrix[i][j];
                }
            }

            meanR/=weight;      meanG/=weight;    meanB/=weight;

            tempR[y][x]=round(meanR); tempG[y][x]=round(meanG); tempB[y][x]=round(meanB);

        }
    imageR=tempR;
    imageG=tempG;
    imageB=tempB;
    uniteColors();

}


void SIE::sharpen(){//{3}
   QVector < QVector <double> > sharpMatrix;
   sharpMatrix.resize(3);
   for (int i=0;i<3;i++)
       sharpMatrix[i].resize(3);
   sharpMatrix[0][0]=-1; sharpMatrix[0][1]=-2; sharpMatrix[0][2]=-1;
   sharpMatrix[1][0]=-2; sharpMatrix[1][1]=22; sharpMatrix[1][2]=-2;
   sharpMatrix[2][0]=-1; sharpMatrix[2][1]=-2; sharpMatrix[2][2]=-1;

   /*sharpMatrix[0][0]=0; sharpMatrix[0][1]=0; sharpMatrix[0][2]=0;
   sharpMatrix[1][0]=0; sharpMatrix[1][1]=1; sharpMatrix[1][2]=0;
   sharpMatrix[2][0]=0; sharpMatrix[2][1]=0; sharpMatrix[2][2]=0;
   */
   runFilt(sharpMatrix);


}

void SIE::gaussianBlur(){//Gaussian Blur fileter {4}
    bool ok;
    double sigma = QInputDialog::getDouble(this, "Set Sigma:0.2 to 5.0",
                                      "Sigma:", 1.0, 0.2, 5.0, 4, &ok);
    if (!ok) return;
    QVector < QVector <double> > gausMatrix;
    int matSize=qRound(6*sigma);
    if (matSize % 2 ==0) matSize-=1;
    gausMatrix.resize(matSize);
    for (int i=0; i<matSize; i++)
        gausMatrix[i].resize(matSize);

   for (int y=0;y<=(matSize/2);y++)
        for (int x=0; x<=(matSize/2);x++){
            double vol=1/(2*3.141592*sigma*sigma)*exp(-(x*x+y*y)/(2*sigma*sigma));
            gausMatrix[matSize/2+y][matSize/2+x]=vol;
            gausMatrix[matSize/2+y][matSize/2-x]=vol;
            gausMatrix[matSize/2-y][matSize/2+x]=vol;
            gausMatrix[matSize/2-y][matSize/2-x]=vol;
         }
   runFilt(gausMatrix);
}

void SIE::biLinealInterpol(double x,double y,int &rvalue,int &gvalue,int &bvalue){//private function
    int xl=qFloor(x), xh=xl+1, yl=qFloor(y), yh=yl+1;    //for scale() and rotate()
    double rval=0,gval=0,bval=0;
    if (xl<0 || xl>=image.width() || yl<0 || yl>=image.height()){
        rval+=0; gval+=0; bval+=0;
    }
    else{
        rval+=imageR[yl][xl]*(xh-x)*(yh-y);
        gval+=imageG[yl][xl]*(xh-x)*(yh-y);
        bval+=imageB[yl][xl]*(xh-x)*(yh-y);
    }
    if (xh<0 || xh>=image.width() || yl<0 || yl>=image.height()){
        rval+=0; gval+=0; bval+=0;
    }
    else{
        rval+=imageR[yl][xh]*(x-xl)*(yh-y);
        gval+=imageG[yl][xh]*(x-xl)*(yh-y);
        bval+=imageB[yl][xh]*(x-xl)*(yh-y);
    }
    if (xl<0 || xl>=image.width() || yh<0 || yh>=image.height()){
        rval+=0; gval+=0; bval+=0;
    }
    else{
        rval+=imageR[yh][xl]*(xh-x)*(y-yl);
        gval+=imageG[yh][xl]*(xh-x)*(y-yl);
        bval+=imageB[yh][xl]*(xh-x)*(y-yl);
    }
    if (xh<0 || xh>=image.width() || yh<0 || yh>=image.height()){
        rval+=0; gval+=0; bval+=0;
    }
    else{
        rval+=imageR[yh][xh]*(x-xl)*(y-yl);
        gval+=imageG[yh][xh]*(x-xl)*(y-yl);
        bval+=imageB[yh][xh]*(x-xl)*(y-yl);
    }

    rvalue=rval; gvalue=gval; bvalue=bval;
}

void SIE::scaling(){//Zoom in & zoom out {5}
bool ok;
    double scale = QInputDialog::getDouble(this, "Set Scale factor",
                                      "type scale factor(from 0.3 to 3):", 1.0, 0.3, 3.0, 3, &ok);
    if (!ok) return;
    QVector < QVector <double> > tempR,tempG,tempB;
    setVsize(tempR);
    setVsize(tempG);
    setVsize(tempB);

    int rval=0,gval=0,bval=0;
    for (int y=0;y<image.height();y++)
        for (int x=0;x<image.width();x++){
            double oldx=1/scale*(x-image.width()/2.0)+image.width()/2.0;
            double oldy=image.height()/2.0-1/scale*(image.height()/2.0-y);
            biLinealInterpol(oldx,oldy,rval,gval,bval);
            tempR[y][x]=rval;
            tempG[y][x]=gval;
            tempB[y][x]=bval;
        }
    imageR=tempR;
    imageG=tempG;
    imageB=tempB;
    uniteColors();
}

void SIE::rotate(){//{6}
    bool ok;
    double fi = QInputDialog::getDouble(this, "Set Rotate factor",
                                      "type rotate angle (from -180 to 180):", 0, -180, 180, 1, &ok);
    if (!ok) return;
    fi=fi*3.141592/180;
    QVector < QVector <double> > tempR,tempG,tempB;
    setVsize(tempR);
    setVsize(tempG);
    setVsize(tempB);
    int rval=0,gval=0,bval=0;
    for (int y=0;y<image.height();y++)
        for (int x=0;x<image.width();x++){
            double xmid=x-image.width()/2.0;
            double ymid=-y+image.height()/2.0;

            double xbackrot=qCos(fi)*xmid-qSin(fi)*ymid;
            double ybackrot=qSin(fi)*xmid+qCos(fi)*ymid;

            double xold=xbackrot+image.width()/2.0;
            double yold=image.height()/2.0-ybackrot;

            biLinealInterpol(xold,yold,rval,gval,bval);
            tempR[y][x]=rval;
            tempG[y][x]=gval;
            tempB[y][x]=bval;
        }
    imageR=tempR;
    imageG=tempG;
    imageB=tempB;
    uniteColors();

}

void SIE::makeMedianFilt(QVector < QVector <double> > & ColorM,int radius){
    QVector < QVector <double> > tempM;
    QVector <double> sortM;
    setVsize(tempM);
    for (int y=0;y<image.height();y++)
        for (int x=0;x<image.width();x++){

            for (int i=-radius; i<=radius; i++){
                if  (y+i<0 || y+i>=image.height()) continue;
                for (int j=-radius; j<=radius;j++){
                     if (x+j<0 || x+j>=image.width()) continue;
                     sortM.push_back(ColorM[y+i][x+j]);
                }
            }
            for (int i=0; i<sortM.size(); i++)
                for (int j=1; j<sortM.size()-i; j++)
                    if (sortM[j]<sortM[j-1]){
                        double tmp=sortM[j];
                        sortM[j]=sortM[j-1];
                        sortM[j-1]=tmp;
                    }

            tempM[y][x]=sortM[sortM.size()/2];
            sortM.clear();
        }
    ColorM=tempM;
}


void SIE::medianFilt(){
    bool ok;
    int radius = QInputDialog::getInt(this, "Set Median Filter",
                                          "type filter radius(from 1 to 4):", 1, 1, 4, 1,&ok);
     if (!ok) return;
    makeMedianFilt(imageR,radius);
    makeMedianFilt(imageG,radius);
    makeMedianFilt(imageB,radius);
    uniteColors();
}

void SIE::grayWorld(){
    double meanR=0, meanG=0,meanB=0;
    for (int y=0; y<image.height();y++)
        for (int x=0; x<image.width(); x++ ){
            meanR+=imageR[y][x];
            meanG+=imageG[y][x];
            meanB+=imageB[y][x];
        }
    int imagePsize=image.width()*image.height();
    meanR/=imagePsize; meanG/=imagePsize; meanB/=imagePsize;
    double avr=(meanR+meanG+meanB)/3.0;
    for (int y=0; y<image.height();y++)
        for (int x=0; x<image.width(); x++ ){
            imageR[y][x]=imageR[y][x]*avr/meanR;
            imageG[y][x]=imageG[y][x]*avr/meanG;
            imageB[y][x]=imageB[y][x]*avr/meanB;
        }
    uniteColors();
}

void SIE::glass(){
    bool ok;
    int radius = QInputDialog::getInt(this, "Glass Effect",
                                          "type radius(from 1 to 10):", 1, 1, 10, 1,&ok);
     if (!ok) return;

    QImage tempImage(image);


    for (int y=0; y<image.height();y++)
        for (int x=0; x<image.width(); x++ ){
            int xold=x+qRound((qrand()*1.0/RAND_MAX - 0.5)*radius);
            int yold=y+qRound((qrand()*1.0/RAND_MAX - 0.5)*radius);

            if (xold<-0) xold*=-1;
            if (xold>=image.width()) xold=2*image.width()-2-xold;
            if (yold <-0) yold*=-1;
            if (yold>=image.height()) yold=2*image.height()-2-yold;

            tempImage.setPixel(x,y,image.pixel(xold,yold));
        }
   image=tempImage;
   imageLabel->setPixmap(QPixmap::fromImage(image));
   extractColors();
}
